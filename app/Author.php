<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
